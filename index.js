// npm init
// npm install express

let express = require("express");
  // load express module to be able to use express properties and methods
let PORT = 4000;
let app = express();
  // app is now our server


// middlewares
app.use(express.json()); // allow your app to read json data
app.use(express.urlencoded({extended:true})) // allow your app to read data from the forms




// routes


app.get("/", (req, res) => {res.send(`Hello World`)
});

// mini activity 
  // show a route that will show a message "Hello from the /hello endpoint"
app.get("/hello", (req, res) => {res.send(`Hello from the /hello endpoint`)
});
//  mini activitty 
  // create a greeting route that will send request data to the server
  // display dynamic message depending on the value of the request body data
  // message should be "Hello there, Joy!"
  // if data changed, the response message should display the corresponding changed value
  // example: "Hello there, miah"


app.post("/greeting", (req, res) => {

  // console.log(`Hello there, Joy!`)
  console.log(req.body)
  res.send(`Hello there, ${req.body.firstName}!`)
});



app.listen(PORT, () => {
  console.log(`Server Running at port ${PORT}`)
});